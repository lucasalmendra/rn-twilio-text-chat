import { NativeModules, Platform } from 'react-native';

const {
  TwilioChatMessages,
} = NativeModules;

class Message {

  constructor(props, channelSid) {
    console.log("props ", props)
    this.sid = props.sid;
    this.index = props.index;
    this.author = props.author;
    this.body = props.body;
    this.type = props.type;
    this.timestamp = Platform.OS === 'ios' ? new Date(props.timestamp) : new Date(Math.floor(props.timestamp / 1000) * 1000);
    this.dateUpdated = props.dateUpdatedAsDate ? new Date(props.dateUpdated) : null;
    this.lastUpdatedBy = props.lastUpdatedBy;
    this.attributes = props.attributes;
    this._channelSid = channelSid;
  }

  updateBody(body) {
    return TwilioChatMessages.updateBody(this._channelSid, this.index, body);
  }

  setAttributes(attributes) {
    return TwilioChatMessages.setAttributes(this._channelSid, this.index, attributes);
  }
}

export default Message;